<!-- BEGIN_TF_DOCS -->
# Introduction

TODO (EDIT ONLY IN [./docs-source/headers.md](./docs-source/headers.md) file)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | 1.6.4 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

No inputs.

## Outputs

No outputs.
<!-- END_TF_DOCS -->
