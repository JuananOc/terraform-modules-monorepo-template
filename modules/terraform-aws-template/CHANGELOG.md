# Changelog

## template-module-v1.0.0 (2023-11-25)


### Features

* **terraform-aws-template:** generate the default module with the makefile actions ([fa64bf1](https://gitlab.com/JuananOc/terraform-modules-monorepo-template/commit/fa64bf11a3da1f4f72fd08db6e5e7ed71a271b28))
