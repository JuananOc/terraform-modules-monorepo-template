# terraform-modules-monorepo-template

## Description

This repository is prepared to store multiple moduules of terraform inside the same repository, prepared to generate local releases for each of them using conventional commits

## Getting started

1. Create a `envrc` file
```shell
make env > .envrc
```

2. Update the environment variables required

3. Load the environment

```shell
make setup
```

## Installation

In the `make setup` command it will check that you have all the dependencies required to start working.

Repository usage:
- make
- python
- pipenv

Release:
- npm
- npx

Terraform:
- tfenv
- terraform
- terraform fmt
- terraform tflint
- terraform docs

Terragrunt:
- tgenv
- terragrunt

## Usage

Check the actions to perform in the repository using the command:

```shell
make help
```

To generate the release from local:

```shell
make run-release
```
