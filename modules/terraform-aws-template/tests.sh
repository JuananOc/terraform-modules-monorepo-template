#!/bin/bash -e

verbose="false"

# Colors definition
BLACK="$(tput setaf 0)"
RED="$(tput setaf 1)"
GREEN="$(tput setaf 2)"
YELLOW="$(tput setaf 3)"
BLUE="$(tput setaf 4)"
MAGENTA="$(tput setaf 5)"
CYAN="$(tput setaf 6)"
WHITE="$(tput setaf 7)"
NOCOLOR="$(tput sgr0)"

# Test paths
submodules_path="./modules"
tests_path="tests"

################################
# Check if parameters options  #
# are given on the commandline #
################################
while :; do
  case "$1" in
  -v | --verbose)
    verbose="true"
    shift 1
    ;;
  --) # End of all options
    shift
    break
    ;;
  -*)
    echo "Error: Unknown option: $1" >&2
    exit 1
    ;;
  *) # No more options
    break
    ;;
  esac
done

# Test loop
echo -e "${CYAN}Testing submodules...${NOCOLOR}"
for test_folder in ${submodules_path}/*/${tests_path}/*/; do
  if [ ${test_folder} = "${submodules_path}/*/${tests_path}/*/" ]; then
    echo -e "${YELLOW}No submodules detected${NOCOLOR}"
    break
  fi
  test=$(basename ${test_folder})
  module=$(basename $(dirname $(dirname ${test_folder})))
  cd ${test_folder}
  [ ${verbose} = "true" ] && echo -e "${YELLOW}Testing ${module} - ${test}...${NOCOLOR}"
  set +e
  if output=$(terragrunt plan -input=false 2>&1); then
    [ ${verbose} = "true" ] && echo "${output}"
    echo -e "${GREEN}[OK] ${module} - ${test}${NOCOLOR}"
  else
    [ ${verbose} = "true" ] && echo "${output}"
    echo -e "${RED}[FAIL] ${module} - ${test}${NOCOLOR}"
  fi
  set -e
  cd - >/dev/null
done

echo -e "${CYAN}Testing main module...${NOCOLOR}"
for test_folder in ${tests_path}/*/; do
  if [ ${test_folder} = "${tests_path}/*/" ]; then
    echo -e "${RED}No tests detected${NOCOLOR}"
    exit 1
  fi
  test=$(basename ${test_folder})
  cd ${test_folder}
  [ ${verbose} = "true" ] && echo -e "${YELLOW}Testing ${test}...${NOCOLOR}"
  set +e
  if output=$(terragrunt plan -input=false 2>&1); then
    [ ${verbose} = "true" ] && echo "${output}"
    echo -e "${GREEN}[OK] ${test}${NOCOLOR}"
  else
    [ ${verbose} = "true" ] && echo "${output}"
    echo -e "${RED}[FAIL] ${test}${NOCOLOR}"
  fi
  set -e
  cd - >/dev/null
done

echo -e "${CYAN}Tests finished${NOCOLOR}"
