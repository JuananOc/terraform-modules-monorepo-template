.DEFAULT_GOAL := help

# The .PHONY directive in a Makefile indicates that a target is not a real file.
# It ensures that the target is always executed, regardless of whether a file with the same name exists.
# In this Makefile, the .PHONY directive is used to declare targets that perform specific tasks and do not generate output files.
# Targets declared as .PHONY will run every time they are invoked, regardless of file existence.
.PHONY: setup test env unset show_env help

## Show available targets and their descriptions.
help:
	@echo "Usage: make [target]"
	@echo ""
	@echo "Targets:"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
			helpMessage = match(lastLine, /^## (.*)/); \
			if (helpMessage) { \
				split(lastLine, helpArray, "## "); \
				printf "  \033[36m%-20s\033[0m %s\n", $$1, helpArray[2]; \
			} \
		} \
		{ lastLine = $$0 }' $(MAKEFILE_LIST)

# --------------------------
# Environment Variable steps
# --------------------------

export GITLAB_TOKEN ?= ""

export MODULES_FOLDER_NAME ?= modules
export TEMPLATE_TERRAFORM_DIRNAME ?= terraform-aws-template

export TERRAFORM_VERSION ?= 1.6.4
export NEW_TERRAFORM_MODULE ?= new-module

export TERRAGRUNT_VERSION ?= 0.53.6

## Set the environment variables redirecting it to a file like .envrc
env:
	@echo 'export GITLAB_TOKEN=${GITLAB_TOKEN}'
	@echo 'export MODULES_FOLDER_NAME=${MODULES_FOLDER_NAME}'
	@echo 'export TEMPLATE_TERRAFORM_DIRNAME=${TEMPLATE_TERRAFORM_DIRNAME}'
	@echo 'export NEW_TERRAFORM_MODULE=${NEW_TERRAFORM_MODULE}'
	@echo 'export TERRAFORM_VERSION=${TERRAFORM_VERSION}'
	@echo 'export TERRAGRUNT_VERSION=${TERRAGRUNT_VERSION}'

## Unset the environment variables
unset:
	@unset GITLAB_TOKEN
	@unset MODULES_FOLDER_NAME
	@unset TEMPLATE_TERRAFORM_DIRNAME
	@unset NEW_TERRAFORM_MODULE
	@unset TERRAFORM_VERSION
	@unset TERRAGRUNT_VERSION

## show-env: Show passed environment to targets
show-env:
	@env

# -------------
# General steps
# -------------

## Setup the repository requirements and check that have all the dependencies installed
setup: setup-release setup-python setup-tf setup-tg


## Perform all the validation checks in the terraform modules
validate: run-pre-commit run-tests-tg

# -------------
# Release steps
# -------------

## Setup the release process
setup-release:
	@echo "Setup the release process"
	@echo "-------------------------"
	@echo "NPM version: $(shell npm --version)"
	@echo "NPX version: $(shell npx --version)"
	@echo
	@echo "Install release dependencies"
	@echo "----------------------------"
	@npm install -D semantic-release-monorepo@v7.0.5 semantic-release@v19.0.5 @semantic-release/changelog@v6.0.3 conventional-changelog-conventionalcommits@v6.1.0 @semantic-release/git@v10.0.1
	@echo

## Execute the release process in every module
run-release:
	@echo "Generating release for all modules"
	@echo "----------------------------------"
	-@for module in $(shell find $(MODULES_FOLDER_NAME) -type f -name "package.json" -exec dirname {} \;); do \
		make release-module-$${module}/; \
	done
	@echo

## Execute the release process in a specific module with the name provided after the release-module. `make release-module-<PATH-TO-MODULE-FROM-REPO-ROOT>`
release-module-%/:
	cd $* && npx semantic-release --no-ci && cd -
	@echo

## Clean release intallation
clean-release:
	@echo "Cleaning release installation"
	@echo "-----------------------------"
	@rm -rf node_modules/
	@echo


# ------------
# Python setup
# ------------

## Setup Terraform dependencies to develop
setup-python:
	@echo "Ensure Python installed"
	@echo "-----------------------"
	@echo "Python version: $(shell python3 --version)"
	@echo "Pipenv version: $(shell pipenv --version)"
	@echo
	@echo "Setup Pipfile"
	@echo "--------------"
	@pipenv install --dev
	@echo
	@echo "Pre-commit version:"
	@echo "-------------------"
	@pipenv run pre-commit --version
	@echo
	@echo "Setup the pre-commit"
	@echo "--------------------"
	@pipenv run pre-commit install-hooks
	@pipenv run pre-commit install
	@pipenv run pre-commit install --hook-type commit-msg
	@echo

## Execute all pre-commit tasks
run-pre-commit:
	@echo "Run pre commit tasks"
	@echo "--------------------"
	@pipenv run pre-commit run --all-files
	@echo

## Clean Terraform dependencies
clean-python:
	@echo "Clean Virtual environment"
	@echo "-------------------------"
	pipenv --rm
	@echo

# ---------------
# Terraform steps
# ---------------

## Create a new terraform module based on the NEW_TERRAFORM_MODULE environment variable
create-tf-module:
	@echo "Create terraform module from template"
	@echo "-------------------------------------"
	cp -r $(MODULES_FOLDER_NAME)/$(TEMPLATE_TERRAFORM_DIRNAME) modules/$(NEW_TERRAFORM_MODULE)
	sed -i '' -e 's/template-module/$(NEW_TERRAFORM_MODULE)/g' modules/$(NEW_TERRAFORM_MODULE)/package.json
	@echo

## Setup Terraform dependencies to develop
setup-tf:
	@echo "Set Terraform Version to use"
	@echo "----------------------------"
	@echo $(TERRAFORM_VERSION) > .terraform-version
	@echo
	@echo "Setup Terraform"
	@echo "---------------"
	@tfenv --version
	@echo

# ----------------
# Terragrunt steps
# ----------------

## Setup Terragrunt dependencies to develop
setup-tg:
	@echo "Set Terragrunt Version to use"
	@echo "-----------------------------"
	@echo $(TERRAGRUNT_VERSION) > .terragrunt-version
	@echo
	@echo "Setup Terragrunt"
	@echo "----------------"
	@tgenv --version
	@echo

## Execute terraform tests of all the modules
run-tests-tg:
	@echo "Executing Terraform tests for all modules"
	@echo "-----------------------------------------"
	-@for module in $(shell find $(MODULES_FOLDER_NAME) -type f -name "tests.sh" -not -path "*/.terragrunt-cache/*" -exec dirname {} \;); do \
		make test-tg-module-$${module}/; \
	done
	@echo

## Execute module tests
test-tg-module-%/:
	cd $* && ./tests.sh
	@echo

## Clean Terragrunt folders
clean-tg:
	@rm -rf "**/.terragrunt-cache"
	@echo
