# File with general information across all tests
# ----------------------------------------------

locals {
  # -----------------
  # General variables
  # -----------------
}

# --------
# Provider
# --------

# Generate the main provider file, it will override other files
generate "main_provider" {
  path      = "provider_override.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "aws" {
  region  = "eu-west-1"
  profile = "account-profile"
}
EOF
}


remote_state {
  backend = "local"
  config = {
    path = "${get_parent_terragrunt_dir()}/${path_relative_to_include()}/terraform.tfstate"
  }

  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}
