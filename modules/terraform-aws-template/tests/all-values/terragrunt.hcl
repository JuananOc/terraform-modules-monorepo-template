# ----------------
# Test description
# ----------------

terraform {
  source = "../../"
}

include "root" {
  path           = "../terragrunt.hcl"
  expose         = true
  merge_strategy = "deep"
}

inputs = {
}
