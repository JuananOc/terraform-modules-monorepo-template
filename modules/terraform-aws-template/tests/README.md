# Tests

## Requirements

In order to develop and execute the tests you'll need:

* Terragrunt and Terraform on the versions indicated in the `.terraform-version` and `.terragrunt-version` files.
* A script to be able to get the credentials from Keycloak for the account/profile specified in the root `terragrunt.hcl`

## How do these tests works

We're using Terragrunt as a wrapper for the tests as it allows us to make different tests with a common source of configuration. The tests structure is as follows:

```txt
└── tests
    ├── terragrunt.hcl
    ├── .terraform-version
    ├── .terragrunt-version
    ├── <test-1-name>
    │   └── terragrunt.hcl
    ├── <test-2-name>
    │   └── terragrunt.hcl
    ...
    └── <test-n-name>
        └── terragrunt.hcl

```

* Root `terragrunt.hcl`: This file contains all the possible values the module can have and the necessary provider configuration. The tests will reference this file to set the values of each one, this way we can have a common source of configuration for all the tests without having to duplicate our code.
* `.terraform-version` and `.terragrunt-version` files indicate the respective versions to be used for the tests of Terraform and Terragrunt.
* Test `terragrunt.hcl`: This file, necessary for each test we want, will indicate which variables from the root `terragrunt.hcl` will be used for the tests.

When using only 1 test, this format might result a bit complex, but this allows us to:

* Scale better in the number of tests, as we rarely will need only 1, allowing us to test multiple conditions at once.
* Test in advance the integration with Terragrunt that will be needed when deploying the module in `infrastructure-code-generator`

## How to execute the tests automatically

To execute all the tests of this repository automatically, from the root of the repository you can execute `make tests` to perform all the tests of this repository, both for the main module and all its submodules. To enable a more verbose output of the tests, showing the generated outputs and possible errors, you can execute `make tests-verbose`.

## How to execute a test manually

You can execute a single test manually you first need to have a valid set of credentials for the account/profile specified in the root `terragrunt.hcl`. Then, you can go to the test folder and execute `terragrunt plan`.

```bash
cd <test-folder>
terragrunt plan
```
